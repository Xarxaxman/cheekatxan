﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PataArmy : MonoBehaviour {

	private const int INITIAL_PATAS = 10;
	private const int START_X = -29;

	private const int MOV_STOP = 0;
	private const int MOV_WALK = 1;
	private const int MOV_ATTACK = 2;
	private const int MOV_DANCE = 3;

	private int movement;

	private float farthestPosition;

	private ArrayList patas;
	public GameObject selector;

	// Use this for initialization
	void Start () {

		farthestPosition = START_X;
		movement = MOV_STOP;
		patas = new ArrayList ();
		spawnPatas ();
		
	}
	
	// Update is called once per frame
	void Update () {
			
		if (Input.GetKey (KeyCode.UpArrow))
			walk ();
		else if (Input.GetKey (KeyCode.DownArrow))
			attack ();
		else if (Input.GetKey (KeyCode.Space))
			stop ();
		else if (Input.GetKey (KeyCode.RightArrow))
			dance ();
		
	}
		
	void spawnPatas() {

		for (int i = 0; i < INITIAL_PATAS; i++) {

			System.Random r = new System.Random ();
			float x = START_X + i*0.6f + (float)r.NextDouble() * 0.4f;
			float yOffset = (float) r.NextDouble () * 0.75f;
			Vector3 spawnPosition = new Vector3 (x, -3.3f + yOffset, 0);
			GameObject pata = Instantiate (selector, spawnPosition, Quaternion.identity) as GameObject;
			pata.transform.localScale = Vector3.one;
			pata.GetComponent<PataScript>().init ();
			patas.Add (pata);

		}

	}

	public void walk() {

		movement = MOV_WALK;
		foreach (GameObject pata in patas)
			pata.GetComponent<PataScript>().walk();

	}

	public void attack() {

		movement = MOV_ATTACK;
		foreach (GameObject pata in patas)
			pata.GetComponent<PataScript>().angryWalk(0.4f);
		
	}

	public void stop() {

		if (movement == MOV_STOP)
			return;
		movement = MOV_STOP;
		foreach (GameObject pata in patas) {
			float pos = pata.GetComponent<PataScript> ().stop ();
			if (pos > farthestPosition)
				farthestPosition = pos;
		}

	}

	public void dance() {

		movement = MOV_DANCE;
		foreach (GameObject pata in patas)
			pata.GetComponent<PataScript>().dance();

	} 

	public float getFarthestPosition() {
		return farthestPosition;
	}

	public ArrayList getPatas(){
		return patas;
	}
}
