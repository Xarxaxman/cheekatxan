﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PataScript : MonoBehaviour {

	private const int MOV_STOP1 = 0;
	private const int MOV_DANCE = 1;
	private const int MOV_WALK = 2;
	private const int MOV_WALKANGRY = 3;
	private const int MOV_STOP2 = 4;

	private int movement;

	private float timeRunAttack;

	public GameObject bowSelector;
	public GameObject arrowSelector;

	private Animator anim;
	int angryWalkHash = Animator.StringToHash("AngryWalk");
	int walkHash = Animator.StringToHash("Walk");
	int stop1Hash = Animator.StringToHash("Stop1");
	int stop2Hash = Animator.StringToHash("Stop2");
	int danceHash = Animator.StringToHash("Dance");

	public void init() {

		timeRunAttack = 0f;

		GameObject bow = Instantiate (bowSelector, this.transform.position + new Vector3(0.3f, 0.1f, 0f), Quaternion.identity) as GameObject;
		bow.transform.parent = this.transform;
		bow.gameObject.SetActive (true);

		anim = GetComponent<Animator>();
		this.gameObject.SetActive(true);
		stop ();
	
	}

	void Update() {

		switch (movement) {
			
		case MOV_WALK:
			transform.Translate (Vector3.right * Time.deltaTime);
			break;
		case MOV_WALKANGRY:
			transform.Translate (Vector3.right * 1.25f * Time.deltaTime);
			timeRunAttack -= Time.deltaTime;
			break;
		
		}

		if (movement == MOV_WALKANGRY && timeRunAttack <= 0f) {
			dance ();
			attack ();
			timeRunAttack = 0f;
		}

	}

	// returns final position
	public float stop() {

		movement = chooseStop();
		print (movement);
		if (movement == MOV_STOP1)
			anim.SetTrigger (stop1Hash); 
		else 
			anim.SetTrigger (stop2Hash);

		return this.transform.position.x;


	}

	public void dance() {

		anim.SetTrigger (danceHash);
		movement = MOV_DANCE;

	}

	public void walk() {

		anim.SetTrigger (walkHash);
		movement = MOV_WALK;

	}

	public void angryWalk(float timeRun) {

		timeRunAttack = timeRun;
		anim.SetTrigger (angryWalkHash);
		movement = MOV_WALKANGRY;

	}

	public void attack() {

		GameObject arrow = Instantiate (arrowSelector, this.transform.position + new Vector3(1f, 0.1f, 0f), Quaternion.identity) as GameObject;
		arrow.gameObject.SetActive (true);

	}

	private int chooseStop() {
		
		System.Random r = new System.Random();
		if (r.Next (0, 100) % 3 == 0) // prioritize stop1
			return MOV_STOP2;
		else
			return MOV_STOP1;
		
	}

}
