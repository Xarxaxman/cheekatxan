﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour {

	private PataArmy pataArmy;
	private float initialPosition;

	// Use this for initialization
	void Start () {

		pataArmy = GameObject.Find("GameController").GetComponent<PataArmy> ();
		initialPosition = this.transform.position.x;

	}
	
	// Update is called once per frame
	void Update () {

		float farthestPos = -100.0f;
		foreach(GameObject pata in pataArmy.getPatas()) {
			if (pata.transform.position.x > farthestPos)
				farthestPos = pata.transform.position.x;
		}

		this.transform.position = new Vector3 (Mathf.Max(farthestPos, initialPosition), this.transform.position.y, this.transform.position.z);
		
	}
}
