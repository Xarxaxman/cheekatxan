﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameController : MonoBehaviour {

	private const int BOTTOM_LEFT = 0;
	private const int TOP_LEFT = 1;
	private const int TOP_RIGHT = 2;
	private const int BOTTOM_RIGHT = 3;

	private const float secondsTempo = 0.5f;
	private const float tolerance = 0.1f;
	float lastkeypress = 0;

	private float startedtowalk=0;
	private float walktime=0;
	private float startedtoattack=0;
	private float attacktime=0;
	private float startedtodance=0;
	private float dancetime=0;

	private Tempo mytempo;
	private GUIUtils gui;
	private PataArmy patarmy;



	void Start() {

		mytempo = GetComponent<Tempo>();
		gui = GetComponent<GUIUtils> ();

		mytempo= GetComponent<Tempo>();
		patarmy = GetComponent<PataArmy> ();

	}

	void Update() {

		keyLogic ();
		checkFX ();


	}

	void checkFX(){
		if (mytempo.flash > 0) {
			gui.playFlashForSeconds (mytempo.flash);
			mytempo.flash = 0;
		}
		if (mytempo.walk > 0 ) {
			startedtowalk = Time.time;
			walktime = mytempo.walk;
			patarmy.walk ();
			mytempo.walk = 0;
		}
		if (Time.time > walktime + startedtowalk && walktime!=0) {
			walktime = 0;
			patarmy.stop ();
		}
		if (mytempo.attack > 0) {
			startedtoattack = Time.time;
			attacktime = mytempo.attack;
			patarmy.attack ();
			mytempo.attack = 0;
		}
		if (Time.time > attacktime + startedtoattack && attacktime !=0) {
			attacktime = 0;
			patarmy.stop ();
		}
		if (mytempo.dance > 0) {
			//print ("begin to dance");
			startedtodance = Time.time;
			dancetime = mytempo.dance;
			patarmy.dance ();
			mytempo.dance = 0;
		}
		if (Time.time > dancetime + startedtodance && dancetime !=0) {
			//print ("stopping to dance");
			dancetime = 0;
			patarmy.stop ();
		}
	}


	void keyLogic() {
		///MAP
		///  Chika Chan
		///  Tlin Plash

		float currentkeypressed = Time.time * 100;
		if (currentkeypressed - lastkeypress <= 35)return;

		if (Input.GetMouseButtonDown(0)) {
			lastkeypress = currentkeypressed;
			print("Touch");
			//          Vector2 position = Input.GetTouch(0).position;
			Vector2 position = Input.mousePosition;
			float x = position.x; float y = position.y;
			if(pixelLocation(x,y)==0) mytempo.keyPressed ("klin");
			if(pixelLocation(x,y)==1) mytempo.keyPressed ("chika");
			if(pixelLocation(x,y)==2) mytempo.keyPressed ("chan");
			if(pixelLocation(x,y)==3) mytempo.keyPressed ("plash");

			//print (pixelLocation (x, y));


		}
		if (Input.GetKey (KeyCode.Q)) {
			mytempo.keyPressed ("chika");
			lastkeypress = currentkeypressed;

		}
		if (Input.GetKey (KeyCode.S)) {
			mytempo.keyPressed ("plash");
			lastkeypress = currentkeypressed;

		}
		if (Input.GetKey (KeyCode.W)) {
			mytempo.keyPressed ("chan");
			lastkeypress = currentkeypressed;
		}
		if (Input.GetKey (KeyCode.A)) {
			mytempo.keyPressed ("klin");
			lastkeypress = currentkeypressed;
		}
	}

	int pixelLocation(float x, float y) {

		float xLocation = x / Screen.width;
		float yLocation = y / Screen.height;

		if (xLocation <= 0.5) {

			if (yLocation <= 0.5)
				return BOTTOM_LEFT;

			return TOP_LEFT;

		} else {

			if (yLocation > 0.5)
				return TOP_RIGHT;

			return BOTTOM_RIGHT;

		}

	}

}