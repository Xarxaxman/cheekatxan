﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

	private const float secondsShot = 2.0f;
	private const float distanceX = 10.0f;

	private float degrees;
	private float initialXPos;
	private float initialYPos;

	// Use this for initialization
	void Start () {

		degrees = 0.25f * Mathf.PI;
		initialXPos = this.transform.position.x;
		initialYPos = this.transform.position.y;

		transform.eulerAngles = new Vector3(0, 0, -45);

	}
	
	// Update is called once per frame
	void Update () {

		if (degrees >= Mathf.PI * 0.75f) {
			Destroy (this.gameObject);
		}

		float percTime = Time.deltaTime / secondsShot;
		float offsetDegrees = percTime * (Mathf.PI / 2.0f);
		degrees += offsetDegrees;

		float offsetX = percTime * distanceX;
		this.transform.position = new Vector3 (this.transform.position.x + offsetX, initialYPos + Mathf.Sin (degrees), 0f);

		float deg = offsetDegrees * Mathf.Rad2Deg;
		this.transform.Rotate (0, 0, -deg);

	}
}
