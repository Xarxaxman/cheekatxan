﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tempo : MonoBehaviour {

	public AudioClip Drumbase;
	public AudioClip Chika;
	public AudioClip Chan;
	public AudioClip Klin;
	public AudioClip Plash;

	public AudioClip Error;
	public AudioClip TimingError;

	private AudioSource audioSource;

	public ArrayList response;
	bool responding;
	bool preresponding;

	bool goodTiming;
	private const float tempo = 80;
	private const float tolerance = 10;


	bool donedrum=true;
	public void setMelody(int id){
		print (id);
	}


	private float lastsuccess = 0;
	private float lastinput =0;

	private ArrayList walking_combo= new ArrayList{"chika","chika","klin","chan"};//walking
	private ArrayList attacking_combo= new ArrayList{"klin","chan","chan","plash"};//attacking
	private ArrayList dancing_combo= new ArrayList{"klin","chan","plash","klin"};

	//variables to call external effects
	public float flash=0;
	public float attack=0;
	public float walk=0;
	public float dance=0;



	bool equal(ArrayList a, ArrayList b){
		for (int i =0 ; i<a.Count ;i++){
			if(a[i] != b[i]) return false;
		}
		return true;
	}

	public void playSound(string sound){
		if (sound.Equals ("chika"))
			audioSource.PlayOneShot (Chika, 1);
		else if (sound.Equals ("chan"))
			audioSource.PlayOneShot (Chan, 1);
		else if (sound.Equals ("klin"))
			audioSource.PlayOneShot (Klin, 1);
		else if (sound.Equals ("plash"))
			audioSource.PlayOneShot (Plash, 1);
		else
			audioSource.PlayOneShot (Error, 1);
	}
	// Use this for initialization
	void Start ( ){
		audioSource = GetComponent<AudioSource>();
		response = new ArrayList{};//"chika","chika","chika","tlin","plash","chika","chika","tlin","plash","chika","chika","tlin"};
		responding=false;
		preresponding = false;
	}

	// Update is called once per frame
	void Update () {
		if ((int)(Time.time * 100) % tempo < 5) {
			//base drum
			if (!donedrum) {
				audioSource.PlayOneShot (Drumbase, 0.5f);
				if (responding && (Time.time*100 - lastsuccess > 50)) {
					//print ("Time" + Time.time * 100);
					//print ("Last" + lastsuccess);
					playSound ((string)response[0]);
					response.RemoveAt(0);
					if (response.Count == 0)
						responding = false;
				}
				donedrum = true;
			}
		} else {
			donedrum = false;
		}
		//whether you should press button or not
		goodTiming = ((int)(Time.time*100) % tempo) < tolerance || ((int)(Time.time*100) % tempo) > (tempo - tolerance);

		//      print (tempo);

	}

	public void keyPressed(string sound){
		//received everytime a key is pressed
		//print(sound);
		if (goodTiming && !responding) {
			//you did it right
			playSound (sound);
			//print(sound);
			if (Time.time * 100 - lastinput > 120) {
				//this resets the melody, but will be also called every time the music restarts
				response = new ArrayList{};
			}
			lastinput=Time.time*100;
			//this counts for a combo
			response.Add(sound);
			if (response.Count >= 4) {
				if (equal (response, walking_combo)) {
					responding = true;
					lastsuccess = Time.time * 100;
					lastinput = lastinput - 150;
					//begin animation to walk
					flash =2.5f;
					walk = 2.5f;
				}else if(equal(response,attacking_combo)){
					responding = true;
					lastsuccess = Time.time * 100;
					lastinput = lastinput - 150;
					//begin animation to attack
					flash =2.5f;
					attack = 2.5f;
				} else if(equal(response,dancing_combo)){
					responding = true;
					lastsuccess = Time.time * 100;
					lastinput = lastinput - 150;
					//begin animation to dance WOOOOOOOOOOOOO
					flash =2.5f;
					dance = 2.5f;				
				}else {
					//you did it wrong
					playSound("error");
					//break your combo

					response=new ArrayList{};
					responding = false;
				}
			}

		} else {
			//you did it wrong
			playSound("error");
			//print("error");

			//break your combo
			response=new ArrayList{};
			responding = false;
		}
	}

	public int getTempo() {return 4;
	}


}